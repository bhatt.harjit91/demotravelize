//
//  MainTabBarViewController.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTabBar()
        // Do any additional setup after loading the view.
    }
    
    func setupTabBar(){
        let LeadsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeadsViewController") as! LeadsViewController
        let EngagementVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EngagementsViewController") as! EngagementsViewController
        
        let firstTabNavigationController = UINavigationController(rootViewController:LeadsVC)
        
        let secondTabNavigationControoller = UINavigationController(rootViewController: EngagementVC)
        
        self.viewControllers = [firstTabNavigationController, secondTabNavigationControoller]
        
        let item1 = UITabBarItem(title: "Leads", image: UIImage(systemName: "person.3.fill"), tag: 0)
        let item2 = UITabBarItem(title: "Engagements", image:  UIImage(systemName: "figure.stand.line.dotted.figure.stand"), tag: 1)
        
        firstTabNavigationController.tabBarItem = item1
        secondTabNavigationControoller.tabBarItem = item2
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
