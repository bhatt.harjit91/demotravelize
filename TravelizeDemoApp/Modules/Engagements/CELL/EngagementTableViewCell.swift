//
//  EngagementTableViewCell.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import UIKit

class EngagementTableViewCell: UITableViewCell {
    @IBOutlet weak var lblEngagementDataTime: UILabel!
    @IBOutlet weak var lblEngagementType: UILabel!
    @IBOutlet weak var lblAssignedByName: UILabel!
    @IBOutlet weak var lblAssignedToName: UILabel!
    
    var mainData:Engagement?{
        didSet{
            self.lblEngagementDataTime.text = mainData?.engagementDateTime
            self.lblEngagementType.text = mainData?.engagementType
            self.lblAssignedByName.text = mainData?.leadAssignDetails?[0].assignByName
            self.lblAssignedToName.text = mainData?.leadAssignDetails?[0].assignToName
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
