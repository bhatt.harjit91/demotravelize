//
//  EngagementsViewController.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import UIKit
import Combine
class EngagementsViewController: BaseClassViewController {
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var mainTableView: UITableView!
    var apiCancellable: Cancellable? {
        didSet { oldValue?.cancel() }
    }
    var mainData:EngagementDataModelClass?{
        didSet{
            DispatchQueue.main.async {
                self.mainTableView.reloadData()
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Engagement"
        self.apiEngagement()
        self.pulToRefresh()
        // Do any additional setup after loading the view.
    }
    
    func pulToRefresh(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        self.mainTableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc func pullToRefresh(_ sender:AnyObject){
        self.apiEngagement()
        self.refreshControl.endRefreshing()
    }
}
