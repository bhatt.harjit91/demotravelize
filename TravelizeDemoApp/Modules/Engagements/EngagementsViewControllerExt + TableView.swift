//
//  EngagementsViewControllerExt + TableView.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import Foundation
import UIKit
extension EngagementsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainData?.engagements?.count ?? 0
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EngagementTableViewCell", for: indexPath) as! EngagementTableViewCell
        cell.mainData = self.mainData?.engagements?[indexPath.row]
        return cell
    }
     
}
