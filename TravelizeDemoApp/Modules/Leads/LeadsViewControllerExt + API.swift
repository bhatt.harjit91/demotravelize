//
//  LeadsViewControllerExt + API.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//
import SwiftToast
import Foundation
import SwiftyJSON
extension LeadsViewController{
    func apiLeads(){
        self.activityIndicator.startAnimating()
        let url = "\(ApiEndPoints.leads)?stage=All&assigned_to=&sd=2022-5-01&ed=2022-5-11&lead_id=null&sbn_id=4799d6740e71d494b67a23081a4b4889&contact_added_by=All&followup_days_filter=All&uid=32"
        print(url)
        let countryCityPublisher = try! ApiPublishers.apiGet(url: url)
        
        apiCancellable = countryCityPublisher
            .map({$0.data})
            .retry(5)
            .receive(on: DispatchQueue.global(qos: .utility))
            .decode(type: LeadsDataModelClass.self, decoder: JSONDecoder())
            .sink(receiveCompletion: { (completionError) in
                switch completionError {
                case .failure(let error):
                    DispatchQueue.main.async {
                        let toast =  SwiftToast(
                            text: "",
                            textAlignment: .center,
                            duration: 1.0,
                            //  minimumHeight: CGFloat(100.0),
                            target: nil,
                            style: .bottomToTop)
                        self.present(toast, animated: true)
                        self.activityIndicator.stopAnimating()
                    }
                    
                    //self.workItem?.cancel()
                    print(error.localizedDescription)
                case .finished:
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                    }
                    break
                }
            }) { (data) in
                self.mainData = data
                self.apiCancellable?.cancel()
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
                
            }
    }
}

