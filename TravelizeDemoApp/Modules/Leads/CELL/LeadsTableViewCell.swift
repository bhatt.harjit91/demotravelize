//
//  LeadsTableViewCell.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import UIKit

class LeadsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCalendar: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblLeads: UILabel!
    @IBOutlet weak var lblCompanyNames: UILabel!
    var mainData:LeadElement?{
        didSet{
            lblCalendar.text = mainData?.leadDate
            lblLeads.text = mainData?.leadGenation
            lblEmail.text = mainData?.email
            lblMobile.text = mainData?.mobile
            lblCompanyNames.text = mainData?.companyName
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
