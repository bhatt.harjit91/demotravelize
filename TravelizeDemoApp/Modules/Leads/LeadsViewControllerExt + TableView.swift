//
//  LeadsViewControllerExt + TableView.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import Foundation
import UIKit
extension LeadsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainData?.leads?.count ?? 0
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadsTableViewCell", for: indexPath) as! LeadsTableViewCell
        cell.mainData = self.mainData?.leads?[indexPath.row]
        return cell
    }
     
}
