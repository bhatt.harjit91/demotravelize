//
//  AppDelegate.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
var window: UIWindow?

     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window?.rootViewController = MainTabBarViewController()
        return true
    }
 
}

