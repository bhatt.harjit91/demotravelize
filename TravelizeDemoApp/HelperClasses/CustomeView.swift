//
//  CustomeView.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

 
import Foundation
import UIKit


@IBDesignable

class CustomeView:UIView {
    
    @IBInspectable
       var shadowRadius: CGFloat {
           get {
               return layer.shadowRadius
           }
           set {

               layer.shadowRadius = newValue
           }
       }
       @IBInspectable
       var shadowOffset : CGSize{

           get{
               return layer.shadowOffset
           }set{

               layer.shadowOffset = newValue
           }
       }

       @IBInspectable
       var shadowColor : UIColor{
           get{
               return UIColor.init(cgColor: layer.shadowColor!)
           }
           set {
               layer.shadowColor = newValue.cgColor
           }
       }
    
      
       @IBInspectable
       var shadowOpacity : Float {

           get{
               return layer.shadowOpacity
           }
           set {

               layer.shadowOpacity = newValue

           }
       }

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}


struct Spacer{
    enum Flow {
        case Horizontal
        case Vertical
    }
   let flow: Flow
    
   subscript(_ value:CGFloat)-> UIView {
        let view = UIView()
    
        view.translatesAutoresizingMaskIntoConstraints = false
        switch flow {
        case .Horizontal:
            view.widthAnchor.constraint(equalToConstant: value).isActive = true
        default:
            view.heightAnchor.constraint(equalToConstant: value).isActive = true
        }
        view.backgroundColor = .clear
        return view
    }
}
@IBDesignable
class CustomeSearchBar:UISearchBar{
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
