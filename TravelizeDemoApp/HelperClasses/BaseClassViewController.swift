//
//  BaseClassViewController.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//
 
import UIKit

class BaseClassViewController: UIViewController{
    let activityIndicator = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()
        addActivityIndicator()
        // Do any additional setup after loading the view.
    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    
    func showAlert(title:String?,msg:String?,completion: @escaping () -> Void){
        self.resignFirstResponder()
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: "OK", style: .default) { _ in
            return completion()
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
        
    }
     
    func showAlertWithCancel(title:String?,msg:String?,completion: @escaping () -> Void){
        self.resignFirstResponder()
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: "Ok", style: .default) { _ in
            return completion()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
             
        }
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func addActivityIndicator(){
        activityIndicator.transform = CGAffineTransform(scaleX: 2, y: 2)
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
    }
      
}
