//
//  APIEndpoints.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import Foundation
 
struct ApiEndPoints {
  
  private static let baseURL = "https://pythonapi.betaphase.in/"
    
  static let leads = "\(baseURL)leads_bde/leads"
  static let engagements = "\(baseURL)engagements/get_data"
    
}
