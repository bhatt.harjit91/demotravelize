//
//  NetworkPublishers.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import Foundation
 
import SwiftyJSON
enum APIError: Error {
    case invalidBody
    case invalidEndpoint
    case invalidURL
    case emptyData
    case invalidJSON
    case invalidResponse
    case statusCode(Int)
}

import Combine

class ApiPublishers {
    
    static func apiGet(url:String) throws -> URLSession.DataTaskPublisher {
        
        guard let url = URL(string: url ) else {
            throw APIError.invalidEndpoint
        }
        
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: .infinity)
        
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        return session.dataTaskPublisher(for: request)
        
    }
    //MARK: POST
    static func apiPost(url:String,param:AnyObject) throws -> URLSession.DataTaskPublisher {
        
        guard let url = URL(string: url ) else {
            throw APIError.invalidEndpoint
        }
        
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: .infinity)
      //  let jsonObj = JSON(param ?? JSON())
        request.httpBody = getData(json: param)
        //request.httpBody = try! JSONSerialization.data(withJSONObject: param as Any, options: .prettyPrinted)
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        return session.dataTaskPublisher(for: request)
        
    }
    
    static func getData(json : AnyObject) -> Data {
        
        var data : Data?
        if let jsonObj = json as? JSON {
            do {
                data = try jsonObj.rawData()
            } catch { }
        }
        if data == nil {
            let jsonString = JSONString(json)
            data = jsonString.data(
                using: String.Encoding.utf8)!
        }
        return data!
    }
    
    static func JSONString(_ value: AnyObject,prettyPrinted:Bool = false) -> String {
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        if JSONSerialization.isValidJSONObject(value) {
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                print("JSONString error")
            }
            
        }
        return ""
        
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension NSMutableData {
    func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

 
