//
//  EngagementsDataClass.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import Foundation

// MARK: - EngagementDataModelClass
struct EngagementDataModelClass: Codable {
    var engagements: [Engagement]?
}

// MARK: - Engagement
struct Engagement: Codable {
    var id: Int?
    var fName, lName, email, mobile: String?
    var companyName: String?
    var bdeID: Int?
    var leadAssignDetails: [LeadAssignDetail]?
    var engagementType: String?
    var engagementRemarks, engagementDateTime: String?

    enum CodingKeys: String, CodingKey {
        case id
        case fName = "f_name"
        case lName = "l_name"
        case email, mobile
        case companyName = "company_name"
        case bdeID = "bde_id"
        case leadAssignDetails = "lead_assign_details"
        case engagementType = "engagement_type"
        case engagementRemarks = "engagement_remarks"
        case engagementDateTime = "engagement_date_time"
    }
}

 

// MARK: - LeadAssignDetail
struct LeadAssignDetail: Codable {
    var assignToID: Int?
    var assignToName: String?
    var assignToRole: String?
    var assignByID: Int?
    var assignByName: String?
    var assignByRole: String?
    var product: String?
    var dateTime: String?

    enum CodingKeys: String, CodingKey {
        case assignToID = "assign_to_id"
        case assignToName = "assign_to_name"
        case assignToRole = "assign_to_role"
        case assignByID = "assign_by_id"
        case assignByName = "assign_by_name"
        case assignByRole = "assign_by_role"
        case product
        case dateTime = "date_time"
    }
}

 

 
