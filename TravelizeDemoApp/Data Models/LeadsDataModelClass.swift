//
//  LeadsDataModelClass.swift
//  TravelizeDemoApp
//
//  Created by Harjit Singh on 24/08/22.
//

import Foundation
import SwiftyJSON

// MARK: - LeadsDataModelClass
struct LeadsDataModelClass: Codable {
    var leads: [LeadElement]?
    var stagesCount: [LeadEngagement]?
    var engagement: LeadEngagement?

    enum CodingKeys: String, CodingKey {
        case leads
        case stagesCount = "stages_count"
        case engagement
    }
}

// MARK: - Engagement
struct LeadEngagement: Codable {
    var name: String?
    var count: Int?
}

// MARK: - LeadElement
struct LeadElement: Codable {
    var id: Int?
    var fName, lName, email, jobTitle: String?
    var mobile, fax, companyName, industry: String?
    var website: String?
    var currentStage: String?
    var status: Status?
    var bdeName: String?
    var bdeID: Int?
    var leadGenation, lastEngagement: String?
    var lastFollowupdate, lastFollowupdateForCompare: JSON?
    var todayFollowup, todayEngagement: Int?
    var isNullEngagementDate, todayDateFromDB, todayDateFromScript, leadDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case fName = "f_name"
        case lName = "l_name"
        case email
        case jobTitle = "job_title"
        case mobile, fax
        case companyName = "company_name"
        case industry, website
        case currentStage = "current_stage"
        case status
        case bdeName = "bde_name"
        case bdeID = "bde_id"
        case leadGenation = "lead_genation"
        case lastEngagement = "last_engagement"
        case lastFollowupdate = "last_followupdate"
        case lastFollowupdateForCompare = "last_followupdate_for_compare"
        case todayFollowup = "today_followup"
        case todayEngagement = "today_engagement"
        case isNullEngagementDate = "is_null_engagement_date"
        case todayDateFromDB = "today_date_from_db"
        case todayDateFromScript = "today_date_from_script"
        case leadDate = "lead_date"
    }
}

// MARK: - Status
struct Status: Codable {
    var lead: StatusLead?
}

// MARK: - StatusLead
struct StatusLead: Codable {
    var date: String?
}
